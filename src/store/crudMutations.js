import Vue from 'vue'

export default {
  setElements (state, { type, elements }) {
    Vue.set(state, type, { ...elements })
  },
  setElement (state, { type, id, element }) {
    Vue.set(state[type], id, { ...element })
  },
  updateElement (state, { type, id, element }) {
    const oldElement = state.items[id]
    const updatedElement = { ...oldElement, ...element }
    Vue.set(state[type], id, updatedElement)
  },
  removeElement (state, { type, id }) {
    Vue.delete(state[type], id)
  },
}
