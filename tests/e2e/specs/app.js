// https://docs.cypress.io/api/introduction/api.html

describe('App', () => {
  beforeEach(() => {
    cy.loginWithStore({ email: Cypress.env('testUser'), password: Cypress.env('testPass') })
  })

  it('App loads', () => {
    cy.contains('h1', 'time')
  })

  it('Navbar works', () => {
    cy.getBySel('navbar')
    cy.getBySel('navbar-link-weekly').click()
    cy.location('pathname').should('eq', '/')
    cy.getBySel('navbar-link-daily').click()
    cy.location('pathname').should('eq', '/daily')
    cy.getBySel('navbar-link-report').click()
    cy.location('pathname').should('eq', '/report')
  })
})
