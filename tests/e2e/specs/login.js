const testUser = Cypress.env('testUser')
const testPass = Cypress.env('testPass')

describe('Login page', () => {
  beforeEach(() => {
    cy.seedTasks([])
  })

  it('Redirects unauthorized user to login page and returns them to previous location', () => {
    cy.loginWithStore({ email: testUser, password: testPass })

    cy.getBySel('navbar-link-report').click()
    cy.urlEq('/report')

    cy.vuex().invoke('dispatch', 'auth/forceSignOut', {}, { root: true })

    cy.reload()

    cy.urlEq('/login')
    cy.getBySel('login-email').type(testUser)
    cy.getBySel('login-password').type(`${testPass}{enter}`)

    cy.urlEq('/report')
  })

  it('Redirects unauthorized user to login page if api response is 401', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: `${Cypress.env('apiUrl')}/tasks`,
      status: 401,
      response: {},
    })
    cy.loginWithStore({ email: testUser, password: testPass })
    cy.visit('/weekly')
    cy.urlEq('/login')
    cy.getBySel('login-email').type(testUser)
    cy.getBySel('login-password').type(`${testPass}{enter}`)

    cy.urlEq('/weekly')
  })

  it('Redirects authorized user to home page', () => {
    cy.loginWithStore({ email: testUser, password: testPass })
    cy.visit('/login')
    cy.urlEq('/daily')
  })

  it('User can log in', () => {
    cy.userAuthMock()
    cy.visit('/login')
    cy.getBySel('login-email').type(testUser)
    cy.getBySel('login-password').type(`${testPass}{enter}`)

    cy.urlEq('/daily')
  })

  it('Error is shown if login data is not correct', () => {
    cy.visit('/login')
    cy.getBySel('login-email').type('wronguser')
    cy.getBySel('login-password').type(`${testPass}{enter}`)

    cy.urlEq('/login')
    cy.notificationShown('Login failed')
  })

  it('User can navigate to the register page', () => {
    cy.visit('/login')
    cy.getBySel('register-link').click()
    cy.urlEq('/register')
  })

  it('User can log out', () => {
    cy.loginWithStore({ email: testUser, password: testPass })
    cy.getBySel('navbar-link-logout').click()
    cy.urlEq('/login')
  })
})
