import moment from 'moment'

const taskTitle = 'Sample task'
const allocatedTime = '7200'

describe('Single task card', () => {
  beforeEach(() => {
    cy.server()
    cy.route('GET', `${Cypress.env('apiUrl')}/tasks`, {
      data: [{
        id: 1,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 0,
        taskDate: moment().format(),
        projectId: '1',
      }],
      success: true,
    })
    cy.route('DELETE', `${Cypress.env('apiUrl')}/tasks/1`, {
      success: true,
      data: {},
    })
    cy.route('PUT', `${Cypress.env('apiUrl')}/tasks/1`, {
      success: true,
      data: {
        id: 1,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 5 * 60,
        taskDate: moment().format(),
        projectId: '1',
      },
    }).as('putTask')
    cy.loginWithStore({ email: Cypress.env('testUser'), password: Cypress.env('testPass') })
    cy.visit('/daily')
  })

  it('Can be started', () => {
    cy.clock()
    cy.getBySelNested('task-controls', 'btn-start').click()
    cy.getBySelNested('task-controls', 'btn-stop').should('be.visible')
    cy.getBySelNested('task-controls', 'btn-start').should('not.be.visible')
    cy.getBySel('task-time-overlay').should('be.visible')
    cy.tick(5000)
    cy.getBySel('task-time-overlay').should('contain', '0:00:05')
  })

  it('Can be stopped', () => {
    cy.getBySelNested('task-controls', 'btn-start').click()
    cy.getBySel('task-time-overlay').click()
    cy.getBySel('task-time-overlay').should('not.be.visible')
    cy.getBySelNested('task-controls', 'btn-start').should('be.visible')
  })

  it('Saves time on stop', () => {
    cy.getBySelNested('task-controls', 'btn-start').click()
    cy.getBySel('task-time-overlay').click()
    cy.get('@putTask').should('have.property', 'status', 200)
  })

  it('Can be edited', () => {
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-title-input').should('be.visible')
    cy.getBySelNested('task-date-input-container', 'date-input').should('be.visible')
    cy.getBySel('task-time-allocated-input').should('be.visible')
    cy.getBySel('task-time-spent-input').should('be.visible')
  })

  it('Edit can be canceled and returns to initial state', () => {
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-title-input').type('bla bla bla')
    cy.getBySelNested('task-controls', 'btn-cancel').click()
    cy.getBySel('card-task-1')
      .should('contain', taskTitle)
  })

  it('Can be deleted', () => {
    cy.getBySelNested('task-controls', 'btn-delete').click()
    cy.getBySelLike('card-task')
      .should('have.length', 0)
  })

  it('Parses the time correctly', () => {
    // 1 to 9 -> to hours
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type('2{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '2:00')
    // 10 on to mins
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type('10{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '0:10')
    // .1 to .9 -> to 60 min * x
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type('.5{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '0:30')
    // 1.1 to 1.9 -> 60min + to 60 min * x
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type('1.5{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '1:30')
    // :1 to :59 to mins
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type(':30{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '0:30')
    // 1:20 to 1:20
    cy.getBySelNested('task-controls', 'btn-edit').click()
    cy.getBySel('task-time-allocated-input').clear().type('1:30{enter}')
    cy.getBySel('task-time-allocated-preview').should('contain', '1:30')
  })
})

describe('Multiple task card', () => {
  beforeEach(() => {
    cy.server()
    cy.route('GET', `${Cypress.env('apiUrl')}/tasks`, {
      data: [{
        id: 1,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 0,
        taskDate: moment().format(),
        projectId: '1',
      }, {
        id: 2,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 0,
        taskDate: moment().format(),
        projectId: '1',
      }],
      success: true,
    })
    cy.route('PUT', `${Cypress.env('apiUrl')}/tasks/1`, {
      data: {
        id: 1,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 0,
        taskDate: moment().format(),
        projectId: '1',
      },
      success: true,
    })
    cy.route('DELETE', `${Cypress.env('apiUrl')}/tasks/1`, {
      success: true,
      data: {},
    })
    cy.loginWithStore({ email: Cypress.env('testUser'), password: Cypress.env('testPass') })
    cy.visit('/daily')
  })

  it('Starting task stops other tasks', () => {
    cy.getBySelNested('card-task-1', 'btn-start').click()
    cy.getBySelNested('card-task-2', 'btn-start').click()
    cy.getBySelNested('card-task-1', 'task-time-overlay').should('not.be.visible')
    cy.getBySelNested('card-task-1', 'btn-start').should('be.visible')
  })
})
