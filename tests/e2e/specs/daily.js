import moment from 'moment'

const taskTitle = 'Sample task'
const allocatedTime = '3600'

describe('Daily page', () => {
  beforeEach(() => {
    cy.seedTasks([])
    cy.loginWithStore({ email: Cypress.env('testUser'), password: Cypress.env('testPass') })
    cy.visit('/daily')
  })

  it('New task can be created', () => {
    const timeNow = moment().format()
    cy.route('POST', `${Cypress.env('apiUrl')}/tasks`, {
      data: {
        id: 1,
        title: taskTitle,
        timeAllocated: allocatedTime,
        timeSpent: 0,
        taskDate: timeNow,
        projectId: '1',
      },
      success: true,
    })

    cy.getBySel('new-task-btn').click()
    cy.getBySel('form-task-title').type(taskTitle)
    cy.getBySel('form-task-allocated-time').type(`${allocatedTime}{enter}`)
    cy.getBySelLike('card-task')
      .should('have.length', 1)
      .and('contain', taskTitle)
  })
})
