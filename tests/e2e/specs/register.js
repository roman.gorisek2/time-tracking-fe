const tempUser = 'temp@test.com'
const tempPass = 'secret'

describe('Register page', () => {
  it('User can navigate to the login page', () => {
    cy.visit('/register')
    cy.getBySel('login-link').click()
    cy.urlEq('/login')
  })

  it('Redirects authorized user to home page', () => {
    cy.loginWithStore({ email: Cypress.env('testUser'), password: Cypress.env('testPass') })
    cy.visit('/register')
    cy.urlEq('/daily')
  })

  it('Error is shown if register data is not correct', () => {
    cy.visit('/register')
    cy.getBySel('login-email').type(tempUser)
    cy.getBySel('login-password').type(`${tempPass}-different`)
    cy.getBySel('login-password2').type(`${tempPass}{enter}`)

    cy.urlEq('/register')
    cy.notificationShown('Registration failed')
  })

  it('User can register', () => {
    cy.userAuthMock()
    cy.seedTasks([])
    cy.visit('/register')
    cy.getBySel('login-email').type(tempUser)
    cy.getBySel('login-password').type(`${tempPass}`)
    cy.getBySel('login-password2').type(`${tempPass}{enter}`)

    cy.urlEq('/daily')
    // cy.vuex().then(store => {
    //   cy.callApi('DELETE', `/users/${store.state.auth.user.id}`)
    // })
  })
})
