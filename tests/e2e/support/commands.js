// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('getBySel', (selector, ...args) => {
  return cy.get(`[data-cy="${selector}"]`, ...args)
})

Cypress.Commands.add('getBySelLike', (selector, ...args) => {
  return cy.get(`[data-cy*=${selector}]`, ...args)
})

Cypress.Commands.add('getBySelNested', (selector1, selector2, ...args) => {
  return cy.get(`[data-cy="${selector1}"]`, ...args).find(`[data-cy="${selector2}"]`)
})

Cypress.Commands.add('seedTasks', (seedData = 'fixture:tasks') => {
  cy.server()
  cy.route('GET', `${Cypress.env('apiUrl')}/tasks`, {
    data: seedData,
    success: true,
  })
})

Cypress.Commands.add('vuex', () => cy.window().its('app.$store'))

Cypress.Commands.add('userAuthMock', () => {
  cy.server()
  cy.route('POST', `${Cypress.env('apiUrl')}/auth/login`, {
    success: true,
    token: 'agsdkhgsdjkfghhagdgakdjghajhdkasjhdkjahsd.ahdklahsdkj',
    user: {
      id: 1,
      email: 'test@test.com',
      createdAt: '2021-01-16T20:16:00.189Z',
      updatedAt: '2021-01-16T20:16:00.189Z',
    },
  })
  cy.route('POST', `${Cypress.env('apiUrl')}/auth/register`, {
    success: true,
    token: 'agsdkhgsdjkfghhagdgakdjghajhdkasjhdkjahsd.ahdklahsdkj',
    user: {
      id: 1,
      email: 'test@test.com',
      createdAt: '2021-01-16T20:16:00.189Z',
      updatedAt: '2021-01-16T20:16:00.189Z',
    },
  })
})

Cypress.Commands.add('loginWithStore', (data) => {
  cy.userAuthMock()
  cy.visit('/')
  cy.vuex().invoke('dispatch', 'auth/signInWithEmailAndPassword', data, { root: true })
  cy.visit('/')
})

Cypress.Commands.add('callApi', (method, url, data = {}) => {
  cy.vuex().then(store => {
    cy.request({
      url: `${Cypress.env('apiUrl')}${url}`,
      method,
      body: data,
      auth: {
        bearer: store.state.auth.jwt,
      },
    })
  })
})

Cypress.Commands.add('notificationShown', message => {
  cy.contains('.iziToast-message', message)
})

Cypress.Commands.add('urlEq', path => {
  cy.location('pathname').should('eq', path)
})
